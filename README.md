# BaseRaiders Addon #

This is the back-end of the BaseRaiders game-mode. It establishes a connection to the database and houses several functions required for the game-mode to function properly.