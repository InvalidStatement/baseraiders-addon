local meta = FindMetaTable( "Player" )

function meta:IsSubAdmin()

	if ( self:IsSuperAdmin() ) then return true end
	if ( self:IsAdmin() ) then return true end
	if ( self:IsUserGroup("moderator") ) then return true end

	return false
	
end

local banLengths = {}
banLengths[1] = {"5 Minutes",5}
banLengths[2] = {"30 Minutes",30}
banLengths[3] = {"1 Hour",1 * 60}
banLengths[4] = {"12 Hours",12 * 60}
banLengths[5] = {"1 Day",1440}
banLengths[6] = {"2 Days",1440 * 2}
banLengths[7] = {"4 Days",1440 * 4}
banLengths[8] = {"1 Week",1440 * 7}
banLengths[9] = {"2 Weeks",1440 * 14}
banLengths[10] = {"3 Weeks",1440 * 21}
banLengths[11] = {"1 Month",1440 * 31}
banLengths[12] = {"Permaban",0}




local banCache = {}
function getBans(callb)

	http.Fetch("http://www.coinerd.com/dam.php?a=banlist",function(str,leng,headers,code)
		str = string.Trim(str)
		str = string.gsub(str,"  "," ")
		
		local t = string.Explode(" ",str)
		--play it safe and remove entries that should really never show up 
		for i,v in pairs(t) do
			if string.len(v) < 2 then table.remove(t,i) end
		end

		banCache = {}
		while(t[2]) do
			if string.len(t[1]) > 0 && string.len(t[2]) > 0 then
				local name = dec(string.gsub(string.gsub(t[1],"\n","")," ",""))
				local steamid = dec(string.gsub(string.gsub(t[2],"\n","")," ",""))
				table.insert(banCache,{name,steamid})
				
			end
			table.remove(t,1)
			table.remove(t,1)
		end
		
		if callb then
			callb()
		end
		
	end,function(err) LocalPlayer():ChatPrint("Failed to retrieve bans "..err) end)

	

end
getBans()



function FillHistory(pnl,id)
	http.Fetch("http://www.coinerd.com/dam.php?a=history&id="..id,function(str)
		str = string.Trim(str)
		local t = string.Explode(" ",str)
		
		while(t[4]) do
			
			if t[1] && t[2] && t[3] && t[4] then
			
			
				local action = dec(t[1])
				local reason = dec(t[2])
				local tim = timeformat(tonumber(t[3]))
				local admin = dec(t[4])
			
				pnl:AddLine(action,reason,tim,admin)
				
				table.remove(t,1)
				table.remove(t,1)
				table.remove(t,1)
				table.remove(t,1)
			end
		end
		

	end)
end

--this next bit is not mine

-- Lua 5.1+ base64 v3.0 (c) 2009 by Alex Kloss <alexthkloss@web.de>
-- licensed under the terms of the LGPL2

local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

function dec(data)
	if !data then return "" end
    return (data:gsub('.', function(x)
        if (x == '=') then return '00' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
   end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)        
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end

--end not mine

local selectedPlayer = NULL
local bannedSteamID = ""


local banName = ""
local banExpire = 0
local banAdmin = ""
local banMessage = ""



local PANEL = {}
function PANEL:Init()
	self:SetSize(640,480)
	self:SetTitle("The Command Center")
	self:Center()
	self:MakePopup()
	gui.EnableScreenClicker(true)
	
	--Create panels and add as a sheet first
	self.playerList = vgui.Create("DPanel") --holds player list
	self.banList = vgui.Create("DPanel")	--holds banlist on left
	self.miscCommands	= vgui.Create("DPanelList") --holds other misc commands
	self.miscCommands.Paint = function() end
	self.miscCommands:SetSpacing(10)
	
	self.bannedInfo = vgui.Create("DPanel") --for already banned players
	self.BansReloading = false
	self.bannedInfo.Paint = function()
		if !banExpire then 
			if !self.BansReloading then
				self:ReloadBans() 
				self.BansReloading = true 
			end
			
			return 
		end
		
		local banExpireText = banExpire
		if tonumber(banExpire) == 0 then
			banExpireText = "NEVER (Permabanned)"
		end
		
		draw.SimpleText(banName,"ScoreboardSub",5,10,Color(0,0,0,255),0,1)
		
		draw.SimpleText("["..bannedSteamID.."]","Default",5,25,Color(0,0,0,255),0,1)
		draw.SimpleText("Banned By: "..banAdmin,"Default",5,40,Color(255,0,0,255),0,1)
		draw.SimpleText(banMessage,"Default",5,55,Color(0,0,0,255),0,1)
		draw.SimpleText("Expires: "..banExpireText,"Default",5,70,Color(0,0,0,255),0,1)
	end
	
	self.banPlayer  = vgui.Create("DPanel") --for a bad player about to be banned
	self.playerHistory  = vgui.Create("DPanel") --for a bad player about to be banned
	self.playerMisc	= vgui.Create("DPanel") --holds other misc commands relevant to the selected playuer
	self.manBan = vgui.Create("DForm",self) --holds the manual ban panel
	self.manBan:SetVisible(false)
	self.manBan:SetName("Manual Banning System")
	local steamID = self.manBan:TextEntry("SteamID")
	local reasonChoice = self.manBan:TextEntry("Ban Reason")
	

	
	
	local choice = self.manBan:ComboBox("Ban Length")
	for i,v in ipairs(banLengths) do
		choice:AddChoice(v[1],v[2])
	end
	
	--lazy way of adding a spacer, dunno a better way
	local spacer = vgui.Create("DPanel")
	spacer.Paint = function() end
	spacer:SetTall(30)
	self.manBan:AddItem(spacer)
	
	local minutes = self.manBan:TextEntry("Minutes")
	minutes:SetValue(1)
	
	local hours = self.manBan:TextEntry("Hours")
	hours:SetValue(0)
	
	local days = self.manBan:TextEntry("Days")
	days:SetValue(0)
	
	choice.OnSelect = function(s,index,value,data)
	

		local d = math.floor(data / 1400);
		local h = math.floor((data % 1440) / 60);
		local m = math.floor(data % 60);
		
		days:SetValue(d)
		hours:SetValue(h)
		minutes:SetValue(m)
	
	
	end
	
	
	
	local button = self.manBan:Button("Ban This SteamID")
	button.DoClick = function() local message = reasonChoice:GetValue() RunConsoleCommand("DAM_Ban",steamID:GetValue(),((days:GetValue()*1440)+(hours:GetValue()*60)+minutes:GetValue())*60,message) end

	
	
	self.leftSheet = vgui.Create("DPropertySheet",self)
	self.leftSheet:StretchToParent(5,25,nil,30)
	self.leftSheet:SetWide(250)
	self.leftSheet:AddSheet("Online Players",self.playerList)
	self.leftSheet:AddSheet("Banned Players",self.banList)
	--right now everything in here is admin only (no subadmin)
	if Me:IsAdmin() then
		self.leftSheet:AddSheet("Misc",self.miscCommands)
	end
	
	
	self.pList = vgui.Create("DListView",self.playerList)
	self.bList = vgui.Create("DListView",self.banList)
	
	self.pList.OnRowSelected = function(s,id,Line) selectedPlayer = Line.player self.rightSheetPlayer:InvalidateLayout() end
	self.bList.OnRowSelected = function(s,id,Line) bannedSteamID = Line.steamid self.rightSheetBan:InvalidateLayout() end
	
	
	
	self.pList:AddColumn("Name")
	self.pList:AddColumn("SteamID")
	self.bList:AddColumn("Name")
	self.bList:AddColumn("SteamID")
	
	self.pList:SetMultiSelect(false)
	self.bList:SetMultiSelect(false)
	
	
	
	self.histList = vgui.Create("DListView",self.playerHistory)
	self.histList:AddColumn("Action")
	self.histList:AddColumn("Reason")
	self.histList:AddColumn("Date")
	self.histList:AddColumn("Admin")
	
	
	self.onlineSearch = vgui.Create("DTextEntry",self.playerList)
	self.banSearch = vgui.Create("DTextEntry",self.banList)
	
	local onlineVal = ""
	self.onlineSearch.Think = function()
		local val = self.onlineSearch:GetValue()
		if val == onlineVal then return end
		onlineVal = val
		self.pList:Clear()
		local t = 	{}
		for i,v in pairs(player.GetAll()) do
			if string.find(string.lower(v:Name()),string.lower(val),1,true) then table.insert(t,v) end
		end
		
		for i,v in pairs(t) do
			local line =  self.pList:AddLine(v:Name(),v:GetNWString("SteamID"))
			line.player = v
		end
		self.pList:SelectFirstItem()
	end
	local banVal = ""
	self.banSearch.Think = function()
		local val = self.banSearch:GetValue()
		if val == banVal then return end
		banVal = val
		
		self.bList:Clear()
		local t = 	{}
		for i,v in pairs(banCache) do
			if string.find(string.lower(v[1]),string.lower(val),1,true) then table.insert(t,{v[1],v[2]}) end
		end

		for i,v in pairs(t) do
			local line = self.bList:AddLine(v[1],v[2])
			line.steamid = v[2]
		end
		self.bList:SelectFirstItem()
	end
	
	
	self.rightSheetBan = vgui.Create("DPropertySheet",self)
	self.rightSheetBan:StretchToParent(self.leftSheet:GetWide()+10,25,5,30)
	self.rightSheetBan:AddSheet("Ban Info",self.bannedInfo)
	self.rightSheetBan:SetVisible(false)
	
	
	
	self.rightSheetPlayer = vgui.Create("DPropertySheet",self)
	self.rightSheetPlayer:StretchToParent(self.leftSheet:GetWide()+10,25,5,30)
	if Me:IsAdmin() then
		self.rightSheetPlayer:AddSheet("Ban Player",self.banPlayer)
	else
		self.banPlayer:SetVisible(false)
	end
	self.rightSheetPlayer:AddSheet("Other Commands",self.playerMisc)
	self.rightSheetPlayer:AddSheet("History",self.playerHistory)
	self.rightSheetPlayer:SetVisible(false)
	
	
	self.banForm = vgui.Create("DForm",self.banPlayer)
	local reasonChoice = self.banForm:TextEntry("Ban Reason")
	

	
	
	local choice = self.banForm:ComboBox("Ban Length")
	for i,v in ipairs(banLengths) do
		choice:AddChoice(v[1],v[2])
	end
	
	--lazy way of adding a spacer, dunno a better way
	local spacer = vgui.Create("DPanel")
	spacer.Paint = function() end
	spacer:SetTall(30)
	self.banForm:AddItem(spacer)
	
	local minutes = self.banForm:TextEntry("Minutes")
	minutes:SetValue(1)
	
	local hours = self.banForm:TextEntry("Hours")
	hours:SetValue(0)
	
	local days = self.banForm:TextEntry("Days")
	days:SetValue(0)
	
	choice.OnSelect = function(s,index,value,data)
	

		local d = math.floor(data / 1400);
		local h = math.floor((data % 1440) / 60);
		local m = math.floor(data % 60);
		
		days:SetValue(d)
		hours:SetValue(h)
		minutes:SetValue(m)
	
	
	end
	
	
	
	local button = self.banForm:Button("Ban This Player")
	button.DoClick = function() local message = reasonChoice:GetValue() RunConsoleCommand("DAM_Ban",selectedPlayer:EntIndex(),((days:GetValue()*1440)+(hours:GetValue()*60)+minutes:GetValue())*60,message) end
	
	
	self.cmdList = vgui.Create("DPanelList",self.playerMisc)
	self.cmdList.Paint = function() end
	self.cmdList:SetSpacing(10)
	
	
	local kickButt = vgui.Create("DButton")
	kickButt:SetText("Kick")
	kickButt.DoClick = function()
		
	
		Derma_StringRequest( "Question", 
					"What did this person do?", 
					"Type your answer here!", 
					function( strTextOut ) RunConsoleCommand("DAM_Kick",selectedPlayer:EntIndex(),strTextOut) end,
					function( strTextOut )  end,
					"Kick", 
					"Cancel" )
	
	
	
	end
	self.cmdList:AddItem(kickButt)
	
	local slapButt = vgui.Create("DButton")
	slapButt:SetText("Teleport To")
	slapButt.DoClick = function()
		
		RunConsoleCommand("DAM_TeleportTo",selectedPlayer:EntIndex())
	
	end
	self.cmdList:AddItem(slapButt)
	
--[[
	local slapButt = vgui.Create("DButton")
	slapButt:SetText("Slap")
	slapButt.DoClick = function()
		
		RunConsoleCommand("DAM_Slap",selectedPlayer:EntIndex())
	
	end
	self.cmdList:AddItem(slapButt)
	
	local igniteButt = vgui.Create("DButton")
	igniteButt:SetText("Ignite")
	igniteButt.DoClick = function()
		
		RunConsoleCommand("DAM_Ignite",selectedPlayer:EntIndex())
	
	end
	self.cmdList:AddItem(igniteButt)
	
	local muteButt = vgui.Create("DButton")
	muteButt:SetText("Mute")
	muteButt.DoClick = function()
		
		RunConsoleCommand("DAM_Mute",selectedPlayer:EntIndex())
	
	end
	self.cmdList:AddItem(muteButt)
	]]
	
	
	local warnButt = vgui.Create("DButton")
	warnButt:SetText("Warn")
	warnButt.DoClick = function()
		
		Derma_StringRequest( "Question", 
					"What do you want to warn "..selectedPlayer:Name().." about?", 
					"Stop pissing me off!", 
					function( strTextOut ) RunConsoleCommand("DAM_Warn",selectedPlayer:EntIndex(),strTextOut) end,
					function( strTextOut )  end,
					"Warn", 
					"Cancel" )
	
	end
	self.cmdList:AddItem(warnButt)
	
	
	if Me:IsAdmin() then
		local disguiseButt = vgui.Create("DButton")
		disguiseButt:SetText("Set Disguise")
		disguiseButt.DoClick = function()
			
			Derma_StringRequest( "Question", 
						"Choose a disguise name", 
						"George Foreman", 
						function( strTextOut ) RunConsoleCommand("DAM_Disguise",strTextOut) end,
						function( strTextOut ) RunConsoleCommand("DAM_Disguise") end,
						"Set Disguise", 
						"Remove Disguise")
		
		end
		self.miscCommands:AddItem(disguiseButt)
	end
	
	
	self.unbanButton = vgui.Create("DButton",self.bannedInfo)
	self.unbanButton:SetText("Unban Player")
	self.unbanButton.DoClick = function() RunConsoleCommand("unbanPlayer",bannedSteamID) end
	if !Me:IsAdmin() then self.unbanButton:SetDisabled(true) end
	
	
	
	self.miscCommands.PerformLayout = function()
	
	
	self.manBan:StretchToParent(self.leftSheet:GetWide()+10,25,5,30)
	self.manBan:SetVisible(true)
	self.rightSheetPlayer:SetVisible(false)
	self.rightSheetBan:SetVisible(false)
	
	end
	
	
	self.bannedInfo.PerformLayout = function()
		http.Fetch("http://www.coinerd.com/dam.php?a=baninfo&id="..bannedSteamID,function(txt)
			local t = string.Explode(" ",txt)
			for i,v in pairs(t) do
				t[i] = string.Trim(v)
			end

			banExpire = t[3]
			if tonumber(banExpire) && tonumber(banExpire) != 0 then banExpire = nice_time(banExpire-os.time()) end
			banName = dec(t[1])
			banMessage = dec(t[2])
			banAdmin = dec(t[4])
			
	
		end)
		self.unbanButton:StretchToParent(5,self.bannedInfo:GetTall()-25,5,5)
	
	end
	
	self.playerMisc.PerformLayout = function()
	
		self.cmdList:StretchToParent(5,5,nil,5)
		self.cmdList:SetWide(120)
	
	
	end
	
	self.banPlayer.PerformLayout = function()
		
		self.banForm:StretchToParent(5,5,5,5)
		self.banForm:SetName("Ban "..selectedPlayer:Name())
		
		
	end

	self.playerList.PerformLayout = function()
	
		self.pList:StretchToParent(5,5,5,30)
		self.pList:Clear()
		for i,v in pairs(player.GetAll()) do
			local line =  self.pList:AddLine(v:Name(),v:GetNWString("SteamID"))
			line.player = v
		end
		
		
		self.onlineSearch:StretchToParent(5,self.playerList:GetTall()-25,5,5)
		self.onlineSearch:RequestFocus()
		self.onlineSearch:SelectAllText(true)
		
		self.pList:SelectFirstItem()
		self.rightSheetPlayer:SetVisible(true)
		self.rightSheetBan:SetVisible(false)
		self.manBan:SetVisible(false)
		
		
		
		self.Rcon:StretchToParent(5,self:GetTall()-25,5,5)
	end

	self.banList.PerformLayout = function()
		
		self.bList:StretchToParent(5,5,5,30)
		self.bList:Clear()

		for i,v in pairs(banCache) do
			local line = self.bList:AddLine(v[1],v[2])
			line.steamid = v[2]
		end
		
		self.banSearch:SetPos(5,self.banList:GetTall()-25)
		self.banSearch:SetWide(self.banList:GetWide()-10)

		self.banSearch:RequestFocus()
		self.banSearch:SelectAllText(true)
		
		self.bList:SelectFirstItem()
		self.rightSheetPlayer:SetVisible(false)
		self.rightSheetBan:SetVisible(true)
		self.manBan:SetVisible(false)
		
	end
	
	self.playerHistory.PerformLayout = function()
	
		self.histList:StretchToParent(5,5,5,5)
		self.histList:Clear()
		FillHistory(self.histList,selectedPlayer:SteamID())
		
	end
	
	

	self.Rcon = vgui.Create("DTextEntry",self)
	self.Rcon.OnEnter = function() RunConsoleCommand("DAM_Rcon",self.Rcon:GetValue()) end
	self.Rcon:SetText("Enter RCON Command...")
	local pressed = false
	self.Rcon.OnMousePressed = function() if pressed then return end self.Rcon:SetValue("") pressed = true end
	if !Me:IsAdmin() then self.Rcon:SetVisible(false) end

end




function PANEL:ReloadBans()


	getBans(function()self.banList:InvalidateLayout() self.BansReloading = false end)
	
		
end

function PANEL:Close()

	gui.EnableScreenClicker(false)
	self:Remove()
	
end
vgui.Register("DAM_ClientMenu",PANEL,"DFrame")

local menu
local function showMenu()
	menu = vgui.Create("DAM_ClientMenu")
end
usermessage.Hook("openDAMMenu",showMenu)


usermessage.Hook("damUnban",function(um) 
	local s = um:ReadString()
	for i,v in pairs(banCache) do
		if v[2] == s then
			table.remove(banCache,i)
			break
		end
	end
	menu.banList:InvalidateLayout()
end)

function nice_time(t)
	if t == 0 then return "NEVER" end
	local days = math.floor(t / 86400)
	local hours = math.floor((t % 86400) / 3600)
	local minutes = math.floor((t % 3600) / 60)
	local str = ""
	if days>0 then str = str..days..'D ' end
	if hours>0 then str = str..hours..'H ' end
	if minutes>0 then str = str..minutes..'M ' end
	return str
end

function timeformat(logTime)



		local diff = os.time()-logTime;
		
		
		local minutes = math.floor(diff/60);
		local hours = math.floor(diff/3600);
		local days = math.floor(diff/86400);
		local weeks = math.floor(diff/592200);
		if (minutes < 60 ) then

			if (minutes < 1)  then
				return diff.." seconds ago";
			end
			return minutes..' Minute(s) ago';

		elseif (hours < 24) then

			return hours..' Hour(s) ago';

		elseif (days < 7) then

			return days..' Day(s) ago';

		elseif (weeks < 4) then

			return weeks..' Week(s) ago';

		else

			return os.date('m/d/y',logTime);
		end
		
	return 'WTF';

end


