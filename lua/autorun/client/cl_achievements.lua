--[[
Achievements plugin for Darkland
]]
local myAchievements = {} -- hold earned achievements
local AchievementVars = {} --hold achievement variables


AchievementCount = {} -- hold counts of each type
AchievementCount.All = {} -- holds all of them that are in the current game
AchievementCount.All.Total = 0


local meta = FindMetaTable("Player")
function meta:GetDarklandVar(var,other)
	other = other or 0
	return AchievementVars[var] or other;
end

function meta:HasAchievement(name)
	for i,v in pairs(myAchievements) do
		if i == name then return true end
	end
	return false
end

usermessage.Hook("achievementVars",function( um )

local str = um:ReadString()
local int = um:ReadLong()
AchievementVars[str] = int

end)

--set up totals
hook.Add("OnAchievementAdded","setUpAchievementData",

	function(name,func,IconUrl,GameID,Desc,barFunc)


		AchievementCount[GameID] = AchievementCount[GameID] or {Total = 0}
		
		AchievementCount[GameID].Total = AchievementCount[GameID].Total + 1;
		AchievementCount[GameID].Earned = 0
		
		
		AchievementCount.All.Total = AchievementCount.All.Total + 1;
		AchievementCount.All.Earned = 0
	end
)
hook.Add("Initialize","StartCheckingAchievements",function()
	timer.Simple(10,function()
		timer.Create("achievementChecker",2,0,function()
			for i,v in pairs(achievement.GetAll()) do
				if v.VerifyFunction(Me) && !myAchievements[v.Name] then
					RunConsoleCommand("~achievementEarned",v.Name) --this is verified serverside
				end
			end
		end)
	end)
end)
local function earnedAchievement( um )
	
	local name = um:ReadString()
	myAchievements[name] = true
	local id
	for i,v in pairs(achievement.GetAll()) do
		if v.Name == name then
			id = v.GameID
			break
		end
	end
	if id then
		AchievementCount[id].Earned = AchievementCount[id].Earned + 1
	end
	AchievementCount.All.Earned = AchievementCount.All.Earned + 1
	
	
	
	--show that thing in the corner
	if !g_achievements then return end
	g_achievements:RefreshAchievements(g_achievements.SelectedType)
end
usermessage.Hook("playerEarnedAchievement",earnedAchievement)


function GetAchievement( um ) --only send for achievements you have earned
	local name = um:ReadString()
	myAchievements[name] = true
	
	
	local id
	for i,v in pairs(achievement.GetAll()) do
		if v.Name == name then
			
			id = v.GameID
			break
		end
	end
	if id then
		AchievementCount[id].Earned = AchievementCount[id].Earned + 1
	end
	AchievementCount.All.Earned = AchievementCount.All.Earned + 1
	
end
usermessage.Hook("getAchievement",GetAchievement)

local function show_menu()
	if !IsValid(g_achievements) then
		g_achievements = vgui.Create("Achievements")
		gui.EnableScreenClicker(true)
	elseif g_achievements:IsVisible() then
		g_achievements:SetVisible(false)
		gui.EnableScreenClicker(false)
	else
		g_achievements:SetVisible(true)
		gui.EnableScreenClicker(true)
	end
end
concommand.Add("show_achievements",show_menu)


local PANEL = {}
function PANEL:Init()
	self:SetSize(630,444)
	self:Center()
	self:SetTitle("My Achievements")
	self:SetDraggable(true)
	self:SetSizable(true)
	self:SetBackgroundBlur(true)
	self:SetDeleteOnClose(false)
	self.SelectedType = 0
	local blackpanel = vgui.Create("DPanel",self)
	blackpanel:SetPos(15,42)
	blackpanel:SetSize(self:GetWide()-30,50)
	blackpanel.Paint = function()
		
	
		draw.RoundedBox(0,0,0,blackpanel:GetWide(),blackpanel:GetTall(),Color(32,32,32,255))
		draw.SimpleText("Achievements Earned:","Default",8,10,Color(255,255,255,255),0,4)
		
		local earned = AchievementCount[self.SelectedType].Earned
		local numach = AchievementCount[self.SelectedType].Total
		
		draw.SimpleText(earned.."/"..numach.."( "..math.floor(earned/numach*100).."% )","Default",blackpanel:GetWide()-8,10,Color(255,255,255,255),2,4)
		
		draw.RoundedBox(0,8,blackpanel:GetTall()-24,blackpanel:GetWide()-16 ,16,Color(79,79,79,255))
		draw.RoundedBox(0,8,blackpanel:GetTall()-24,earned/numach*blackpanel:GetWide()-16,16,Color(157,194,80,255))
	end

	self.Types = vgui.Create("DComboBox",self)
	self.Types:SetPos(15,102)
	self.Types:SetSize(225,24)
	self.Types.OnSelect = function(s,ind,val,data) self:RefreshAchievements(data) end
	for i,v in pairs(AchievementCount) do
		
		local strDesc = DarklandGames[i]
		if i == "All" then 
			strDesc = i
		elseif i == 0 then
			strDesc = "Global"
		end
		
		self.Types:AddChoice(strDesc.." ("..v.Earned.." of "..v.Total..")",i) 
	end
	self.aList = vgui.Create("DPanelList",self)
	self.aList:SetPos(15,136)
	self.aList:SetSize(self:GetWide()-30,268)
	self.aList:SetPadding(8)
	self.aList:SetSpacing(5)
	self.aList:EnableVerticalScrollbar()
	self.aList.Paint = function()
		draw.RoundedBox(4,0,0,self.aList:GetWide(),self.aList:GetTall(),Color(15,15,15,255))
		return true
	end
	
	
	self.Types:ChooseOptionID(1)
end
function PANEL:RefreshAchievements(type)
	self.SelectedType = type
	self.aList:Clear()
	for i,v in pairs(self.Types.Choices) do
		local data = self.Types.Data[i]
		local strDesc = DarklandGames[data]
		if data == "All" then 
			strDesc = data
		elseif data == 0 then
			strDesc = "Global"
		end
		self.Types.Choices[i] = strDesc.." ("..AchievementCount[data].Earned.." of "..AchievementCount[data].Total..")" 
	end
	
	for i,v in pairs(achievement.GetAll()) do
		if v.GameID == type || type == "All" then
			local panel = vgui.Create("DPanel")
			panel:SetTall(64)
			local html = vgui.Create("HTML",panel)
			html:SetPos(-8,-8)
			html:SetSize(75,94)
			local code = "<body bgcolor='343434'><center><img width='50' height='50' src='http://www.darklandservers.com/garrysmod/da_awards/q.jpg' /></center></body>"
			if myAchievements[v.Name] then
				code = "<body bgcolor='4f4f4f'><center><img width='50' height='50' src='"..v.IconURL.."' /></center></body>"
			end
			html:SetHTML(code)
			html.PaintOver = function()
			
				if myAchievements[v.Name] then
					surface.SetDrawColor(79,79,79,255)
				else
					surface.SetDrawColor(55,55,55,255)
				end
				surface.DrawLine(html:GetWide()-1,1,html:GetWide()-1,html:GetTall())
			end
			--hide those lines that edge an html panel (only one cause the rest are outside the parent panel)
			--also put this panel over it so it doesnt capture the mouse (so you can scroll down)
			local htmlCover = vgui.Create("Panel",html)
			htmlCover:SetSize(html:GetWide(),html:GetTall())
			htmlCover.Paint = function() end

			
			
			
			panel.Paint = function()
				
				local earned = myAchievements[v.Name]
				
				local col = Color(55,55,55,255)
				if earned then col = Color(79,79,79,255) end
				
				
				draw.RoundedBox(6,0,0,panel:GetWide(),panel:GetTall(),col)
			
				
				col = Color(79,79,79,255)
				
				if earned then col = Color(160,185,104,255) end
				
				draw.SimpleText(v.Name,"Default",70,8,col,0,3)
				
				if earned then col = color_white end
				draw.SimpleText(v.Description,"Default",70,25,col,0,3)
				if v.barFunc then --Draw progess bar if needed
					draw.RoundedBox(0,70,42,350,12,Color(32,32,32,255))
					local percentage = math.min(v.barFunc(Me),1)
					local text = v.myCustomText(Me) --dynamic
					local text2 = v.OutOfCustomText --static
					draw.RoundedBox(0,70,42,percentage*350,12,Color(200,184,148,255))
					draw.SimpleText(text.."/"..text2,"Default",430,48,col,0,1)
				end 
			end
			self.aList:AddItem(panel)
		end
	end


end
function PANEL:Close()
	self:SetVisible(false)
	gui.EnableScreenClicker(false)
end
vgui.Register("Achievements",PANEL,"DFrame")