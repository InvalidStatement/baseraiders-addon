//surface.CreateFont( "Tahoma", 16, 1000, true, false, "DarklandChatBox" )
surface.CreateFont("DarklandChatBox", {font='Tahoma', size=18, weight=1000})
teamchat = false
chaton = false

local realtext = ""
local chattext = ""
local maxlines = 10
local history = {}
local cursorpos = 0

local COLOR_CHAT = Color( 255, 255, 255, 255 )
local COLOR_CONSOLE = Color( 217, 0, 25, 255 )

local prefixes = {}
function AddChatPrefix(index,prefix)
	prefixes[index] = prefix
end

function AddHistory( pl, msg, type,custcol )

	type = tonumber(type) or 0

	
	if IsValid(pl) then
	

			local color = team.GetColor( pl:Team() )
			local chatInfo = {
				pl = pl,
				super = pl:IsSuperAdmin(),
				admin = pl:IsAdmin(),
				moderator = pl:IsSubAdmin(),
				darkspider = pl:GetNWBool("IsDarkspider"),
				israzor = pl:SteamID() == "STEAM_0:1:13222928",
				name = pl:GetName(),
				color = color,
				textcolor = custcol or COLOR_CHAT,
				msg = msg,
				type = type,
				time = RealTime()+10
			};
		if type == 2 then
			chatInfo = {
				pl = pl,
				team = true,
				name = pl:GetName(),
				color = color,
				textcolor = custcol or COLOR_CHAT,
				msg = msg,
				type = type,
				time = RealTime()+10
			};
			chat.AddText(chatInfo.color,pl:GetName()..": ",chatInfo.textcolor,msg)
		end
		table.insert(history,1,chatInfo)
	else
			local chatInfo = {
			textcolor = custcol or COLOR_CONSOLE,
			msg = msg,
			type = type or 1,
			time = RealTime()+10
		};
		table.insert(history,1,chatInfo)
	end
	history[maxlines+1] = nil

end

usermessage.Hook("__chatprint",function(um) local msg = um:ReadString() AddHistory(NULL,msg,0) chat.AddText(Color(255,0,0,255),msg) end)

usermessage.Hook("__chatcust",function(um)
	
	local pl = um:ReadEntity()
	local msg = um:ReadString() 
	
	local red = um:ReadShort()
	local green = um:ReadShort()
	local blue = um:ReadShort()
	
	local custcol = Color(red,green,blue,255)
	AddHistory(pl,msg,0,custcol) 
	if IsValid(pl) then
		chat.AddText(team.GetColor(pl:Team()),pl:GetName(),custcol,msg)
	else
		chat.AddText(custcol,msg)
	end
end
)


function OnPlayerChat( player, msg,t,dead)
	local tab = {}
	
	if ( dead ) then
		table.insert( tab, Color( 255, 30, 40 ) )
		table.insert( tab, "*DEAD* " )
	end
	
	if ( t ) then
		table.insert( tab, Color( 30, 160, 40 ) )
		table.insert( tab, "(TEAM) " )
	end
	
	if ( IsValid( player ) ) then
		if player:GetNWString("disguised") == "" then
		
			table.insert( tab, player )
		else
			local col = team.GetColor(player:Team())
			table.insert( tab, Color( col.r, col.g, col.b ) )
			table.insert( tab, player:GetNWString("disguised") )
		end

		if t then
			AddHistory( player, msg, 2 )
		else
			AddHistory( player, msg, 1 )
		end
	else
		table.insert( tab, "Console" )
		AddHistory( nil, msg, 0 )
	end
	
	table.insert( tab, Color( 255, 255, 255 ) )
	table.insert( tab, ": "..msg )
	
	chat.AddText( unpack(tab) )
	
	return true
end
hook.Add("OnPlayerChat","SeriousChat",OnPlayerChat)
function TW(s,f)
	local w = surface.GetTextSize(s,f)
	return w
end
function util.WordWrap(txt,font,maxl)
	local tab = string.Explode(" ",txt)
	local w = ""
	for i,v in pairs(tab) do
		local t = w.." "..v
		if TW(t,font) < maxl then
			w = t
		else
			w = w.."\n"..v
		end
	end

	return w
end
function ChatPaint()
	
	local x, y = 80, ScrH()-240
	if ChatOffset then 
		x = ChatOffset.x or x 
		y = ChatOffset.y or y 
	end
	local num = 0
	local total = 0
	for i,v in pairs(history) do
		if v.time > RealTime() or chaton then
			if v.pl then
			
				local x2 = x
				local y2 = y
				local namePlus = 0
				if v.team then
					draw.SimpleText("(Team)","DarklandChatBox",x-4,(y-i*15)+1,Color(0,0,0,255),2)
					draw.SimpleText("(Team)","DarklandChatBox",x-5,(y-i*15),Color( 30, 160, 40,255 ),2)
				elseif v.pl:GetNWString("disguised") != "" then
					draw.SimpleText(v.pl:GetNWString("disguised"),"DarklandChatBox",x+1,(y-i*15)+1,Color(0,0,0,255))
					draw.SimpleText(v.pl:GetNWString("disguised"),"DarklandChatBox",x,y-i*15,v.color)	
					x2 = x2+TW(v.pl:GetNWString("disguised")..": ","DarklandChatBox")+1
					namePlus = TW(v.pl:GetNWString("disguised")..": ","DarklandChatBox")+1
				elseif v.darkspider then
					draw.SimpleText("(Owner)","DarklandChatBox",x-4,(y-i*15)+1,Color(0,0,0,255),2)
					draw.SimpleText("(Owner)","DarklandChatBox",x-5,(y-i*15),Color(50,50,FluctuateColor(2)*220,255),2)
				elseif v.super then
					draw.SimpleText("(Super)","DarklandChatBox",x-4,(y-i*15)+1,Color(0,0,0,255),2)
					draw.SimpleText("(Super)","DarklandChatBox",x-5,(y-i*15),Color(0,FluctuateColor(2)*200,0,255),2)
				elseif v.admin then
					draw.SimpleText("(Admin)","DarklandChatBox",x-4,(y-i*15)+1,Color(0,0,0,255),2)
					draw.SimpleText("(Admin)","DarklandChatBox",x-5,(y-i*15),Color(FluctuateColor(2)*255,0,0,255),2)
				elseif v.moderator then
					draw.SimpleText("(Mod)","DarklandChatBox",x-4,(y-i*15)+1,Color(0,0,0,255),2)
					draw.SimpleText("(Mod)","DarklandChatBox",x-5,(y-i*15),Color(FluctuateColor(2)*212,160,0,255),2)
				end
				
				if v.pl:GetNWString("disguised") == "" then
					draw.SimpleText(v.name..": ","DarklandChatBox",x+1,(y-i*15)+1,Color(0,0,0,255))
					draw.SimpleText(v.name..": ","DarklandChatBox",x,y-i*15,v.color)
					namePlus = TW(v.name..": ","DarklandChatBox")+1
					x2 = x2+TW(v.name..": ","DarklandChatBox")+1
				end

				
				
				draw.SimpleText(v.msg,"DarklandChatBox",x+namePlus+1,(y-i*15)+1,Color(0,0,0,255))
				draw.SimpleText(v.msg,"DarklandChatBox",x+namePlus,y-i*15,v.textcolor)
			else
				draw.SimpleText(v.msg,"DarklandChatBox",x+1,(y-i*15)+1,Color(0,0,0,255))
				draw.SimpleText(v.msg,"DarklandChatBox",x,y-i*15,v.textcolor)
			end
		end
	end

	-- Draw text input
	if chaton then
		surface.SetFont( "DarklandChatBox" )
		local prefix = "Say"
		for i,v in pairs(prefixes) do
			if string.find(chattext,i) == 1 then
				prefix = v
				break
			end
		end
		if teamchat then prefix = "Team" end
		
		
		local endstring = prefix..": "..chattext

		local w, h = surface.GetTextSize( endstring )

		if prefix ~= "Cmd" then
			cursorpos = w
		end

		draw.RoundedBox( 4, x-5, y+5, w+14, h+1, Color( 0, 0, 0, 200 ) )

		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( x+2, y+5 )
		surface.DrawText( endstring )

		if ((math.sin(CurTime() * 5) * 10) > 0) then
			surface.SetTextPos( x+cursorpos+1, y+5 )
			surface.DrawText( "|" )
		end

	end

end
hook.Add("HUDPaint","DrawChat",ChatPaint)


	

function StartChat()
	chaton = true
	return true
end
hook.Add("StartChat","SeriousStartChat",StartChat)
function FinishChat()
	chaton = false
	teamchat = false
	chattext = ""
	realtext = ""
end
hook.Add("FinishChat","SeriousFinishChat",FinishChat)
function ChatTextChanged( text )
	realtext = text
	chattext = text
end
hook.Add("ChatTextChanged","ChatMoved",ChatTextChanged)


function StartTeamChat(ply, bind)
	if string.find( bind, "messagemode2" ) then
		teamchat = true
	end
end
hook.Add("PlayerBindPress", "TeamChat", StartTeamChat )

