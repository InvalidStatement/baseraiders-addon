AddCSLuaFile("autorun/sh_darkland.lua")

if (!SERVER) then return; end

require("tmysql4");

Database,errorDBConnect = Database or tmysql.initialize("127.0.0.1", "root", "password", "gmod", 3306);
local queue = {}; // If the sql can't be queried now, we'll queue for when we connect again.
hook.Add("ShutDown","removeconnectiondb",function()
	--Database:Disconnect()
end)
function escape(str)
	return tmysql.escape(str);
end

function Query(sql, callback,flags)

	Database:Query(sql, function(res,stat,err)
		if callback then
			callback(res, stat, err)
		end

		if err ~= 0 and flags ~= QUERY_FLAG_LASTID then
			print("Query error! Error: ", err, ". Sql: ", sql)
		end
	end, flags or QUERY_FLAG_ASSOC)

	return q
end



hook.Add("PlayerInitialSpawn", "AllowFlashLights", function(ply)
	ply:AllowFlashlight( true )
end)


//Create default tables
DEVMODE = false

local cmd = {}
cmd.exec = function(str)
end

hook.Add("Initialize","SendLua",function()

	--add dua for the right folder
	local path 		= util.RelativePathToFull("cl.db")
	local server 	= string.sub(path,24,24)


	if server == "5" then DEVMODE = true end --don't add it on the dev server cause the spam is bad
		
	--old method
	--cmd.exec("copy C:\\SourceServers\\Server"..server.."\\orangebox\\garrysmod\\cache\\dua\\*.dua \"C:\\www\\garrysmod\\cache\\dua\\\"" )
	--cmd.exec("duacopy.exe C:\\SourceServers\\Server"..server.."\\orangebox\\garrysmod\\cache\\dua\\ C:\\www\\garrysmod\\cache\\dua\\")
end)

function LoadMainStuff(pl,tbl,status,err)
	tbl = tbl[1]
	if !tbl then
		tbl = {}
		CreateAccount(pl)
	else
		local steamid = pl:SteamID()
		local sID = GAMEMODE.DarklandID or 0
		Query("UPDATE da_misc SET PlayerName=\'"..escape(pl:Name()).."\',LastLogin="..os.time()..",LastServer="..sID..",Online=1 WHERE SteamID=\'"..steamid.."\'")
	end
	pl:SetNWInt("m_Status",tonumber(tbl['Membership']))
	pl.Loaded 			= true
	pl.AchievementVars = {}
	pl.AchievementsEarned = {}
	if tbl['AchievementVars'] then
		local t = string.Explode("|",tbl['AchievementVars'])
		for i,v in pairs(t) do
			local innerT = string.Explode(":",v)
			pl:SetDarklandVar(innerT[1],tonumber(innerT[2]))
		end
	end
	local steamid = pl:SteamID()
	pl:SetNWString("SteamID",steamid)
	timer.Create("saveAchVars_"..steamid,10,0,function()
		if !IsValid(pl) then timer.Destroy("saveAchVars_"..steamid) return end
		if pl.AchievementVars then
			local t = {}
			for i,v in pairs(pl.AchievementVars) do
				local s = i..":"..v
				table.insert(t,s)
			end
			local finalString = escape(table.concat(t,"|"))
			Query("UPDATE da_misc SET AchievementVars='"..finalString.."' WHERE steamid='"..steamid.."'")
		end
	end)
end




function CreateAccount(pl)
	local steamid = pl:SteamID()
	local name = escape(pl:Name())
	Query("INSERT INTO da_misc (SteamID,PlayerName,Online,LastLogin,LastServer) VALUES (\'"..steamid.."\', \'"..name.."\',1,"..os.time()..",1)")
end

function LoadData(pl,steamid)
	Query("SELECT Membership,AchievementVars FROM da_misc where SteamID=\'"..steamid.."\'",function(res,status,err)
	
		if !IsValid(pl) then return end
		LoadMainStuff(pl,res) 
		hook.Call("PlayerLoadedGlobalProfile",GAMEMODE,pl)
	end,1)
end
hook.Add("PlayerAuthed","aaaaaLoadMainStuff",LoadData)


function FormatIP(ip)
	if ip=="loopback" then
		return "loopback"
	end
	return string.sub(ip,1,string.find(ip,":")-1)
end

local meta = FindMetaTable("Player")

function meta:OriginalName()

	return self:GetNWString("OriginalAdminName","Admin Name Not Set")

end


------
--Remove online players if they are no longer online
------


function ValidateOnlinePlayers(tbl)
	local offlineSteamIDs = {}
	for i,v in pairs(tbl) do
		table.insert(offlineSteamIDs,v["steamid"])
	end
	local onlineIDs = {}
	for i,v in pairs(player.GetAll()) do
		onlineIDs[v:SteamID()] = 1
	end
	for i,v in pairs(offlineSteamIDs) do
		if onlineIDs[v] then offlineSteamIDs[i] = nil end
	end
	if table.Count(offlineSteamIDs) < 1 then return end
	
	local newTbl = {}
	for i,v in pairs(offlineSteamIDs) do
		table.insert(newTbl,"SteamID='"..offlineSteamIDs[i].."'")
	end
	local steamIDString = table.concat(newTbl," or ")
	Query("UPDATE da_misc SET Online=0 WHERE "..steamIDString,function(res,stat,err)print(err) end)
	
end
timer.Create("OnlinePlayerChecker",10,0,function() 
if DEVMODE then return end
local sID = GAMEMODE.DarklandID 
if !sID then return end 
Query("SELECT SteamID FROM da_misc WHERE Online=1 and LastServer="..sID,ValidateOnlinePlayers,1)
end)



-----------------------------
--BANS SYSTEM
---------------------------

function DoUnbans()
	Query("SELECT ID,ExpireTime FROM bans WHERE ExpireTime <> 0",function(res,stat,err)UnbanPlayers(res)end)
end
timer.Create("unbanPlayers",1,0,DoUnbans)
function UnbanPlayers(tbl)
	for i,v in pairs(tbl) do
		local expire = tonumber(v["ExpireTime"])
		if expire < os.time() then
			Query("DELETE FROM bans WHERE ID="..tonumber(v["ID"]))
		end
	end		
end

function CheckBan(pl,tbl)
	if tbl[1] and tbl[1]["SteamID"] then
		if tonumber(tbl[1]["ExpireTime"]) && tonumber(tbl[1]["ExpireTime"]) != 0 then
			if tonumber(tbl[1]["ExpireTime"]) > os.time() then
				timer.Simple(1,function()game.ConsoleCommand("kickid "..pl:UserID().." Banned For "..nice_time(math.max(1,tonumber(tbl[1]["ExpireTime"])-os.time())).."\n")end)
			end
		else
			game.ConsoleCommand("kickid "..pl:UserID().." Banned Forever\n")
		end
	end
end
function LoadBans(pl,steamid) 

	Query("SELECT SteamID,ExpireTime FROM bans where SteamID=\'"..steamid.."\'",function(res,status,err) 
		if !IsValid(pl) then return end 
		CheckBan(pl,res) 
	end)
end
hook.Add("PlayerAuthed","DAM_BanCheck",LoadBans)


----------------------
--AFK SYSTEM
----------------------
local Time = 300 -- 5 mins 
function ResetTimer(ply)
	timer.Start("afkkick_"..ply:SteamID())
end

function CallHook(ply)
	if !IsValid(ply) || DEVMODE then return end
	timer.Destroy("afkkick_"..ply:SteamID())
	gamemode.Call("AFK",ply) --gamemode decides what to do
	--game.ConsoleCommand( Format( "kickid %i %s\n", ply:UserID(), "AFK for "..Time.." seconds" ) )
end

function NewAFKTimer(ply)
	timer.Create("afkkick_" .. ply:SteamID(), Time, 1, function() if IsValid(ply) then CallHook(ply) end end)
end

hook.Add("PlayerInitialSpawn","resettimerspawn",
	function(ply)
		timer.Create("afkkick_" .. ply:SteamID(), Time, 1, function() if IsValid(ply) then CallHook(ply) end end)
	end
)
hook.Add("KeyPress","resettimerkeyPress",ResetTimer)
hook.Add("PlayerSay","resettimerPlySay",ResetTimer)

-----------------
--NO SPAMJOIN
-------------------
local JoinIPS = {}
hook.Add("PlayerConnect","NoSpamJoin",function(name,ip)
	ip = FormatIP(ip)
	JoinIPS[ip] = JoinIPS[ip] or {joinAmt = 0,lastJoin = 0}

	if JoinIPS[ip].joinAmt > 5 then
		if CurTime()-JoinIPS[ip].lastJoin < 1 then
			game.ConsoleCommand("addip 2 "..ip.."\n")
			print("BANNED "..ip.." FOR SPAM JOINING LOL")
		else
			JoinIPS[ip].joinAmt = 0
		end
	end

	
	JoinIPS[ip].lastJoin = CurTime()
	JoinIPS[ip].joinAmt = JoinIPS[ip].joinAmt + 1
end
)


-------------------
--Serverside achievements
-------------------


function MySQLSaveAchievement(name,func,IconUrl,GameID,Desc,barFunc)
	local n = escape(name)
	local i = escape(IconUrl)
	local g = tonumber(GameID)
	local d = escape(Desc)
	
	
	--print(("INSERT INTO achievement_list (GameID,IconURL,Name,Description) VALUES ("..g..",\'"..i.."\',\'"..n.."\',\'"..d.."\') ON DUPLICATE KEY UPDATE Name=Name"))
	Query("INSERT INTO achievement_list (GameID,IconURL,Name,Description) VALUES ("..g..",\'"..i.."\',\'"..n.."\',\'"..d.."\') ON DUPLICATE KEY UPDATE Name=Name")


end
hook.Add("OnAchievementAdded","MySQLSaveAchievement",MySQLSaveAchievement)
function meta:SetDarklandVar(var,val)
	if !self.AchievementVars then return end
	self.AchievementVars[var] = val
	umsg.Start("achievementVars",self)
		umsg.String(var)
		umsg.Long(val)
	umsg.End()
end

function meta:AddDarklandVar(var,amt)
	self.AchievementVars = self.AchievementVars or {}
	self.AchievementVars[var] = self.AchievementVars[var] or 0
	self.AchievementVars[var] = self.AchievementVars[var] + amt
	
	umsg.Start("achievementVars",self)
		umsg.String(var)
		umsg.Long(self.AchievementVars[var])
	umsg.End()

end


function meta:GetDarklandVar(var,other)
	other = other or 0
	if !self.AchievementVars then return other end
	return self.AchievementVars[var] or other;
end

timer.Create("IncreasePlayTime",1,0,
function() 
	for i,v in pairs(player.GetAll()) do 
		v:SetDarklandVar("playtime",v:GetDarklandVar("playtime",0)+1)
	end
end)

hook.Add("ShowSpare2","showAchievementMenu",
	function(pl)
		pl:ConCommand("show_achievements")
	end
)

timer.Create("SaveAchievementVars",10,0,function()
	for i,v in pairs(player.GetAll()) do

	end
end)

function VerifyAchievement(pl,cmd,args)

	local name = args[1]
	local t
	for i,v in pairs(achievement.GetAll()) do
		if v.Name == name then
			t = v
			break
		end
	end
	if !t.VerifyFunction(pl) then return end
	print(pl,"earned",t.Name)
	pl:EarnAchievement(t)


end
concommand.Add("~achievementEarned",VerifyAchievement)

function GiveAchievement(pl,name)


	local t
	for i,v in pairs(achievement.GetAll()) do
		if v.Name == name then
			t = v
			break
		end
	end
	pl:EarnAchievement(t)
end

function meta:HasAchievement(name)
	for i,v in pairs(self.AchievementsEarned) do
		if v == name then return true end
	end
	return false
end

function meta:EarnAchievement(t)
	if self:HasAchievement(t.Name) then return end
	table.insert(self.AchievementsEarned,t.Name)
	
	local name = escape(t.Name)
	Query("INSERT INTO achievements_earned (SteamID,AchievementName) VALUES ('"..self:SteamID().."','"..name.."')")
	umsg.Start("playerEarnedAchievement",self)
		umsg.String(t.Name)
	umsg.End()
	hook.Call("PlayerEarnedAchievement",GAMEMODE,self,t)
end


function LoadAchievements(pl,tbl)
	if !IsValid(pl) then return end
	pl.AchievementsEarned = {}
	for i,v in pairs(tbl) do
		
		table.insert(pl.AchievementsEarned,v.AchievementName)
		umsg.Start("getAchievement",pl)
			umsg.String(v.AchievementName)
		umsg.End()
	end
end
hook.Add("PlayerAuthed","LoadPlayerAchievements",
function(pl,steamid)
local sID = GAMEMODE.DarklandID or 0
Query("SELECT e.AchievementName from achievements_earned AS e,achievement_list AS l WHERE e.SteamID='"..pl:SteamID().."' and l.Name=e.AchievementName and (l.GameID=0 or l.GameID="..sID..")",function(res,s,e)LoadAchievements(pl,res)end,1)

end
)


------------------
--Scriptenforce verify
------------------
local SE
hook.Add("Initialize","SEVar",function()
SE = GetConVarString("sv_scriptenforcer")
end)

function LogMyAss(pl)

	filex.Append("hackerlist.txt",pl:SteamID().." - "..os.date().."\n")
	pl:ConCommand("disconnect")
end
concommand.Add("achievementRefresh",BanAndLogMyAss) --achievementRefresh sounds legit

