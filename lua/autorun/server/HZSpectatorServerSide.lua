function HZSpecLoader(ply)
	--timer.Simple(5,function(ply) if(ply:IsAdmin())then 	ply:SendLua('local s = file.Read([[HZSpectator.lua]], [[LUA]]) if s then RunString(s) print("HZSpec Loaded!") end') end end,ply)
end
hook.Add("PlayerInitialSpawn","HZSPECLOAD",HZSpecLoader)

function HZSpecEnable(ply,cmd,args)
	if(!IsValid(ply) or !ply:IsAdmin())then return end

	local enable = false
	if(args[1] == "true")then
		enable = true
	end
	
	ply.HZSpecOn = enable
end
concommand.Add( "hz_spectate_on", HZSpecEnable)

function HZSpecSelectTarget(ply,cmd,args)
	if(!IsValid(ply) or !ply:IsAdmin())then return end

	if(args[1] and tonumber(args[1]))then
		local target = ents.GetByIndex(tonumber(args[1]))
		if(IsValid(target) and target:IsPlayer())then
			ply.HZSpecTarget = target
		end
	end
end
concommand.Add("hz_spectate_target",HZSpecSelectTarget)

function HZSpecPlyVis(ply)
	if(!IsValid(ply) or !ply:IsAdmin() or !ply.HZSpecOn or !ply.HZSpecTarget)then return end
	local target = ply.HZSpecTarget
	if(!IsValid(target))then return end

	AddOriginToPVS(target:GetPos())
end
hook.Add("SetupPlayerVisibility", "HZSpecPlyVis", HZSpecPlyVis)