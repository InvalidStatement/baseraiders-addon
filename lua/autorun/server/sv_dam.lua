
--Garry's
local SteamIDs = {}

// Load the users file
local UsersKV = util.KeyValuesToTable( file.Read( "settings/users.txt" , "GAME") )

// Extract the data into the SteamIDs table
for key, tab in pairs( UsersKV ) do

	for name, steamid in pairs( tab ) do
	
		SteamIDs[ steamid ] = {}
		SteamIDs[ steamid ].name = name
		SteamIDs[ steamid ].group = key
	
	end

end
--end Garry's


hook.Add("PlayerInitialSpawn","LoadOriginalName",function(pl)
	local steamid = pl:SteamID()
		if steamid == "STEAM_0:0:6114751" then
			pl:SetNWBool("IsDarkspider",true)
		end
	if SteamIDs[steamid] then
		pl:SetNWString("OriginalAdminName",SteamIDs[steamid].name)
	end
end)





commands = commands or {}
hook.Add("Initialize","DAM_Commands",function()

commands["kick"] = DAM_Kick
commands["menu"] = DAM_Menu
commands["rcon"] = DAM_Rcon
commands["ban"] = DAM_Ban
commands["teleport"] = DAM_TeleportTo

end)

local ranks = {}
ranks[1] = "subadmin"
ranks[2] = "admin"
ranks[9] = "superadmin"


local found = 0;
local foundEnt;

local function DAM_Log(pl,type,message,admin)
	local steamid = pl:SteamID()
	local adminSteam = admin:SteamID()
	message = escape(message)
	Query("INSERT INTO history (Action,SteamID,Reason,Time,Admin) VALUES ('"..type.."','"..steamid.."','"..message.."',"..os.time()..",'"..adminSteam.."')")
end

local function RunCommand(pl,txt)
	if string.find(txt,"!") == 1 then
		if !pl:IsSubAdmin() then return "" end
		
		local args = string.Explode(" ",txt)
		args[1] = string.sub(args[1],2)

		
		local cmd = "menu" --args[1]
		table.remove(args,1)
		
		if !commands[cmd] then pl:ChatPrint("That is not a valid DAM Command!") return "" end
		commands[cmd](pl,args)
		return ""
		
	end
end
hook.Add("PlayerSay","DAM_ProcessCommands",RunCommand)



function DAM_Kick(pl,args)

	local kickedPlayer = player.GetByID(args[1])
	if !IsValid(pl) then return end
	if (pl:IsSubAdmin()) then return end
	local message = args[2]
	DAM_Log(kickedPlayer,"Kick",message,pl)
	game.ConsoleCommand("kickid "..kickedPlayer:UserID().." "..message.."\n")


end
concommand.Add("DAM_Kick",function(pl,cmd,args) DAM_Kick(pl,args) end)


function DAM_TeleportTo(pl,args)

	local kickedPlayer = player.GetByID(args[1])
	if !IsValid(pl) then return end
	if (!pl:IsSuperAdmin()) then return end
	pl:SetPos(kickedPlayer:GetPos()-kickedPlayer:GetForward()*50)


end
concommand.Add("DAM_TeleportTo",function(pl,cmd,args) DAM_TeleportTo(pl,args) end)




function DAM_Menu(pl,args)


	SendUserMessage("openDAMMenu",pl)


end

function DAM_Rcon(pl,args)
	if !pl:IsAdmin() then return end
	local str = args[1]
	if string.find(str,"rcon") or string.find(str,"lua") then return end
	game.ConsoleCommand(str.."\n")
end
concommand.Add("DAM_Rcon",function(pl,cmd,args) DAM_Rcon(pl,args) end)

function DAM_Ban(pl,args)
	if !pl:IsAdmin() then return end
	local steamid;
	if tonumber(args[1]) then
		local ply = player.GetByID(args[1])
		if !ply:IsValid() then return end
		steamid = ply:SteamID()
	else
		steamid = escape(args[1])
	end
	
	local seconds = tonumber(args[2])
	local message = escape(args[3])
	local banned_player = player.GetBySteamID(steamid)

	Query("SELECT Membership FROM da_misc WHERE SteamID=\'"..steamid.."\'",
	function(res) DAM_ConfirmBan(steamid,seconds,message,banned_player,pl,res) end)
	
end
concommand.Add("DAM_Ban",function(pl,cmd,args) DAM_Ban(pl,args) end)
 
function DAM_ConfirmBan(steamid,seconds,message,banned_player,admin,res)
	if !admin:IsAdmin() then return end
	local banned_name = "Unknown"
	local abusing_admin = admin:SteamID()
	if res and res[1] and res[1]["Membership"] == "1" then 
		if seconds == 0 then 
			seconds = 5760*60 
		else 
			seconds = math.Clamp(seconds,1,5760*60) 
		end 
	end
	local unban_time = os.time()+seconds
	if seconds == 0 then unban_time = "\'NEVER\'" end	
	
	
	if IsValid(banned_player) then 
		banned_name = escape(banned_player:Name())
		
		game.ConsoleCommand("kickid "..banned_player:UserID().." Banned For:"..nice_time(seconds).." <"..message..">\n")
	end
	Query("INSERT INTO bans (SteamID,PlayerName,ExpireTime,Message,AdminSteamID) VALUES (\'"..steamid.."\',\'"..banned_name.."\',"..unban_time..",\'"..message.."\',\'"..abusing_admin.."\')")
	admin:ChatPrint("Successfully banned "..steamid)
end

local function confirmUnBan(pl,tbl,steamid)

	--non super admins can only unban those they banned
	if tbl[1][1] != pl:SteamID() && !pl:IsSuperAdmin() then return end
	Query("DELETE FROM bans WHERE SteamID='"..steamid.."'")
	pl:ChatPrint("Player successfully unbanned!")
	umsg.Start("damUnban",pl)
		umsg.String(steamid)
	umsg.End()
end

function DAM_Unban(pl,cmd,args)

	if !pl:IsAdmin() then return end
	local steamid = escape(args[1])
	Query("SELECT AdminSteamID FROM bans WHERE SteamID='"..steamid.."'",function(res) confirmUnBan(pl,res,steamid) end)
	

end
concommand.Add("unbanPlayer",DAM_Unban)



function DAM_Warn(pl,cmd,args)

	if !pl:IsSubAdmin() then return end
	local warnedPlayer = player.GetByID(args[1])
	if !IsValid(warnedPlayer) then return end
	local message = args[2] or ""
	DAM_Log(warnedPlayer,"Warn",message,pl)
	warnedPlayer:PrintMessage(4,message.."\nYou have been warned!")
	
	
end
concommand.Add("DAM_Warn",DAM_Warn)

function DAM_Disguise(pl,cmd,args)

	if !pl:IsAdmin() then return end
	local newName = args[1] or ""
	pl:SetNWString("disguised",newName)	
end
concommand.Add("DAM_Disguise",DAM_Disguise)

function nice_time(t)
	if t == 0 then return "NEVER" end
	local days = math.floor(t / 86400)
	local hours = math.floor((t % 86400) / 3600)
	local minutes = math.floor((t % 3600) / 60)
	local str = ""
	if days>0 then str = str..days..'D ' end
	if hours>0 then str = str..hours..'H ' end
	if minutes>0 then str = str..minutes..'M ' end
	return str
end