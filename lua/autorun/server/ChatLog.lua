//require("cmd")
local cmd = {}
cmd.exec = function(str)
end

local path 		= util.RelativePathToFull("cl.db")
local server 	= string.sub(path,24,24)

local currentfilename = nil
	
function LOGME(ply,msg,teamchat)
	if(DEVMODE)then return end
	if(string.sub(msg,1,6) == "/admin")then --dont log admin chat
		return
	end
	//filex.Append("chatlogs/"..currentfilename..".txt",os.date("%m.%d.%y %I:%M:%S %p",os.time()).." Player "..ply:Nick().." Team: ".. team.GetName(ply:Team()) .." ("..ply:SteamID()..") said: "..msg.."\n")
end
hook.Add("PlayerSay","ChatLogPlayerSaid",LOGME)

function STARTLOG()
	if(DEVMODE)then return end
	if(server == 5)then return end
	--finalize logs, in case of crash due on initalize of next game
	local filelist = file.Find("chatlogs/*.txt", "DATA")
	for k,v in pairs(filelist)do
		local firstpart = string.sub(v,1,-5)
		local finalname = firstpart.." - "..string.lower(os.date("%m,%d,%y %I,%M,%S %p",os.time()))..".txt"
		
		--rename to start time end time ; file.Rename does nothing
		cmd.exec("rename \"C:\\SourceServers\\Server"..server.."\\orangebox\\garrysmod\\data\\chatlogs\\"..v.."\" \""..finalname.."\"")
	end

	--copy over last games logs
	cmd.exec("copy C:\\SourceServers\\Server"..server.."\\orangebox\\garrysmod\\data\\chatlogs\\*.txt \"C:\\www\\garrysmod\\chatlogs\\"..GAMEMODE.Name.."\\\"" )

	filelist = file.Find("chatlogs/*.txt", "DATA")
	for k,v in pairs(filelist)do
		file.Delete("chatlogs/"..v) --delete old logs
	end
	currentfilename = os.date("%m,%d,%y %I,%M,%S %p",os.time())
	file.Write("chatlogs/"..currentfilename..".txt","New Game Started At "..currentfilename.."\n") --start a new log
end
hook.Add("Initialize","ChatLogInit",STARTLOG)