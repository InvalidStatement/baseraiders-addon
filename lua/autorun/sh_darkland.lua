DARKLANDBOX = true
--download clientside files
local Files = file.Find("autorun/client/*.lua", "LUA")
for q,w in pairs(Files) do
		if SERVER then AddCSLuaFile("client/"..w) else include("autorun/client/"..w) end
end

local meta = FindMetaTable("Player");
function meta:IsVIP()
	return (self:GetNWInt("m_Status") == 1);
end
function meta:IsSubAdmin()

	if ( self:IsAdmin() ) then return true end
	if ( self:IsUserGroup("moderator") ) then return true end
	//if (self:SteamID() == "STEAM_0:1:53961993") then return true end
	return false
	
end
function meta:ChatPrint(msg)
	if SERVER then
		umsg.Start("__chatprint",self)
		umsg.String(msg)
		umsg.End()
	else
		AddHistory(NULL,msg,0)
		print(msg)
	end
end
if CLIENT then
	function meta:SteamID()
		return self:GetNWString("SteamID")
	end
end
GP = player.GetByID;
function player.GetByUserID(id)
	for i,v in pairs(player.GetAll()) do 
		if v:UserID() == id then return v end
	end
	return nil
end
function player.GetBySteamID(id)
	for i,v in pairs(player.GetAll()) do 
		if v:GetNWString("SteamID") == id then return v end
	end
	return nil
end

DarklandGames 		= {}
DarklandGames[1] 	= "Base Raiders"





meta.oldName = meta.Name
function meta:Name()
	if self:GetNWString("disguised") != "" then return self:GetNWString("disguised") end
	return self:oldName()
end


meta.GetName = meta.Name
meta.Nick = meta.Name


if CLIENT then
	hook.Add("InitPostEntity","CreateMeVar",function()
		Me = LocalPlayer()
	end)
	usermessage.Hook("__chatprint",function(um) local msg = um:ReadString() if DARKLANDCHATBOX then AddHistory(NULL,msg,0) end chat.AddText(Color(255,0,0,255),msg) end)
end

function FluctuateColor(a)
	a = a or 1
	return math.abs(math.sin(CurTime()*a))
end





--------------------------------------
--Achievement system 3.0
--------------------------------------

achievement = {}

local achievements = {}

function achievement.GetAll()
	return achievements
end


--Call(only call shared) like achievement.Add("100 Kills",function(pl) return pl:Frags() > 99 end,"http://www.darklandservers.com/garrysmod/achievements/100_kills.png",1)



function achievement.Add(name,func,IconUrl,GameID,Description,barFunc,myText,OutOfText)


	local t = {}
	t.Name = name --Name of the Achievement (Unique)
	t.VerifyFunction = func -- Function that must work for both server and client (if SERVER then blah else blah end is ok but not recommended)
	t.IconURL = IconUrl --URL of the icon on a website (png preferred)
	t.GameID = GameID --GameID is the DarklandGames index (Fortwars = 1, RP = 2) Use 0 for global (and put all globals in this file)
	t.Description = Description --obvious
	t.barFunc = barFunc --function that returns 0 to 1 based on how close you are to earning the achievement
	t.myCustomText = myText
	t.OutOfCustomText = OutOfText
	
	table.insert(achievements,t)
	hook.Call("OnAchievementAdded",GAMEMODE,name,func,IconUrl,GameID,Description,barFunc)

end


hook.Add("Initialize","loadGlobalAchievements",function()




achievement.Add(
"Cool Kid",
function(pl) return pl:GetDarklandVar("playtime",0) > 3600 end,
"http://www.darklandservers.com/garrysmod/da_awards/CoolKid.png",
0,
"Play for at least one hour",
function(pl) return pl:GetDarklandVar("playtime",0) / 3600 end,
function(pl) return math.min(60,math.floor(pl:GetDarklandVar("playtime")/60)) end,
"60 Minutes")

achievement.Add(
"Losing Life",
function(pl) return pl:GetDarklandVar("playtime",0) > 86400 end,
"http://www.darklandservers.com/garrysmod/da_awards/losinglife.png",
0,
"Play for at least one day",
function(pl) return pl:GetDarklandVar("playtime",0) / 86400 end,
function(pl) return math.min(24,math.floor(pl:GetDarklandVar("playtime")/3600)) end,
"24 Hours")

achievement.Add(
"NO LIFE!",
function(pl) return pl:GetDarklandVar("playtime",0) > 604800 end,
"http://www.darklandservers.com/garrysmod/da_awards/nolife.png",
0,
"Play for at least one week",
function(pl) return pl:GetDarklandVar("playtime",0) / 604800 end,
function(pl) return math.min(7,math.floor(pl:GetDarklandVar("playtime")/86400)) end,
"7 Days")



end)


